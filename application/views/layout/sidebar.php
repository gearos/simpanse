<div class="sidebar-category sidebar-category-visible">
	<div class="category-content no-padding">
		<ul class="navigation navigation-main navigation-accordion">


			<li title="Dashboard">
				<a href="<?=base_url()?>home"><i class="icon-home4"></i>
					<span>Dashboard</span></a>
			</li>
			<li title="Praktisi Medis">
				<a href="<?=base_url()?>praktisimedis"><i class="icon-user-tie"></i>
					<span>Praktisi Medis</span>
				</a>
			</li>
			<li title="Data Pasien">
				<a href="<?=base_url()?>pasien"><i class="icon-user-plus"></i>
					<span>Data Pasien</span>
				</a>
			</li>

		</ul>
	</div>
</div>
