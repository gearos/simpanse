drop database simpanse;
create database simpanse;
use simpanse;

create table users(
	id int primary key not null auto_increment,
	username varchar(25),
	password varchar(255)
); 

create table praktisi_medis(
	id int primary key not null auto_increment,
	kode_praktisi varchar(10),
	nama varchar(50),
	jk enum('L','P'),
	alamat varchar(100),
	hp varchar(15),
	password varchar(255),
	create_at date,
	updated_at date
);


create table pasien(
	id int primary key not null auto_increment,
	kode_pasien varchar(10),
	nama varchar(50),
	jk enum('L','P'),
	umur int,
	berat_badan int,
	riwayat_diabetes double
);

create table dataset(
	id int primary key not null auto_increment
);

create table pemeriksaan(
	id int primary key not null auto_increment,
	tgl_periksa date,
	prediksi int,
	praktisi_medis_id int not null,
	pasien_id int not null,
	dataset_id int not null,
	foreign key (praktisi_medis_id) references praktisi_medis(id),
	foreign key (pasien_id) references pasien(id),
	foreign key (dataset_id) references dataset(id)
);

